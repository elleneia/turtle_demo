"""see https://www.youtube.com/playlist?list=PLE17FF55366ED043E
Python Programming: GENERALIZING PROCEDURES
https://www.mathsisfun.com/geometry/polygons.html
https://www.mathsisfun.com/geometry/interior-angles-polygons.html
ReelLearning
Jun 18, 2012
"""
import turtle

t = turtle.Turtle()
t.showturtle()
t.shape("turtle")
ts = t.getscreen()
ts.bgcolor("black")
t.color("yellow")


def draw_rpolygon(n):
    for number in range(n):
        t.forward(50)
        angle = (180 * (n - 2)) / n
        t.left(180 - angle)  # turn the SUPPLEMENT of the angle, for the interior angle


for i in range(3, 11):
    draw_rpolygon(i)

turtle.exitonclick()
