"""see https://www.youtube.com/playlist?list=PLE17FF55366ED043E
Python Programming: REPETITION & PROCEDURES
ReelLearning
Jun 7, 2012
"""
import turtle

t = turtle.Turtle()
t.shape("turtle")


def draw_square(length):
    for i in range(4):
        t.forward(length)
        t.left(90)


draw_square(100)
draw_square(50)
draw_square(200)

turtle.exitonclick()
