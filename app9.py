""""see https://www.youtube.com/playlist?list=PLE17FF55366ED043E
PythonProgramming: EVENT DRIVEN PROGRAMMING - LISTEN
ReelLearning
Jun 15, 2012
"""
import turtle

t = turtle.Turtle()
ts = t.getscreen()
t.shape("turtle")
t.color("yellow")
ts.bgcolor("black")


# EVENT HANDLERS:
def move():
    t.forward(2)


def turn_left():
    t.left(90)


def turn_right():
    t.right(90)


def spin():
    for i in range(4):
        t.right(90)


# Binding occurred because we used this 'onkey' [when key is released], so the event handler is called
# whenever the specific onkey release happens.
ts.onkey(move, "space")

ts.onkey(turn_left, "Left")

ts.onkey(turn_right, "Right")

ts.onkey(spin, "s")

ts.listen()

turtle.exitonclick()
