"""see https://www.youtube.com/playlist?list=PLE17FF55366ED043E
Python Programming: SEQUENCE (FOR-LOOP)
ReelLearning
Jun 7, 2012
"""
import turtle

t = turtle.Turtle()
ts = t.getscreen()
t.shape("turtle")
# t.forward(100)
# t.left(90)
# t.forward(100)
# t.left(90)
# t.forward(100)
# t.left(90)
# t.forward(100)
# t.left(90)



"""
(For Loop)
for <index> in <sequence>:
    <instructions>
"""

for i in range(4):
    print("turtle")

for i in range(4):
    print(i)

for i in [0, 1, 2, 3]:
    print(i)

for i in range(4):
    t.forward(100)
    t.left(90)

turtle.exitonclick()
