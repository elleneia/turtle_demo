"""see https://www.youtube.com/playlist?list=PLE17FF55366ED043E
Python Programming and the Turtle Module: SEQUENCE (NO-LOOP)
ReelLearning
Jun 7, 2012
"""
import turtle

t = turtle.Turtle()
ts = t.getscreen()
t.shape("turtle")
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)

turtle.exitonclick()