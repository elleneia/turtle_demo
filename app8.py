"""see https://www.youtube.com/playlist?list=PLE17FF55366ED043E
Python Programming: DECISION STRUCTURES
ReelLearning
Jun 18, 2012
"""
import turtle

t = turtle.Turtle()
t.showturtle()
t.shape("turtle")
ts = t.getscreen()
ts.bgcolor("black")
t.color("yellow")


def draw_rpolygon(n):
    for number in range(n):
        t.color(get_angle_color())
        t.forward(10)
        angle = (180 * (n - 2)) / n
        t.left(180 - angle)


# divide circle into 4 quadrants
def get_angle_color():
    if 0 <= t.heading() < 90:
        return "red"
    elif 90 <= t.heading() < 180:
        return "blue"
    elif 180 <= t.heading() < 270:
        return "yellow"
    else:
        return "green"


for i in range(30):   # to approximate a circle
    draw_rpolygon(30)
    t.left(12)


turtle.exitonclick()
